<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220109131138 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rechargement ADD agent_id INT NOT NULL');
        $this->addSql('ALTER TABLE rechargement ADD CONSTRAINT FK_479F0C503414710B FOREIGN KEY (agent_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_479F0C503414710B ON rechargement (agent_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rechargement DROP FOREIGN KEY FK_479F0C503414710B');
        $this->addSql('DROP INDEX IDX_479F0C503414710B ON rechargement');
        $this->addSql('ALTER TABLE rechargement DROP agent_id');
    }
}
