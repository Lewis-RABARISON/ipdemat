<?php

namespace App\Entity;

use App\Repository\BoutRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Rechargement;

/**
 * @ORM\Entity(repositoryClass=BoutRepository::class)
 */
class Bout
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $nRcs;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $numFixe;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $numMobile;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $numTelecopie;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer")
     */
    private $codePost;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $pays;

    /**
     * @ORM\OneToMany(targetEntity=Rechargement::class, mappedBy="bout", orphanRemoval=true)
     */
    private $rechargements;

    public function __construct()
    {
        $this->rechargements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNRcs(): ?int
    {
        return $this->nRcs;
    }

    public function setNRcs(int $nRcs): self
    {
        $this->nRcs = $nRcs;

        return $this;
    }

    public function getNumFixe(): ?string
    {
        return $this->numFixe;
    }

    public function setNumFixe(?string $numFixe): self
    {
        $this->numFixe = $numFixe;

        return $this;
    }

    public function getNumMobile(): ?string
    {
        return $this->numMobile;
    }

    public function setNumMobile(?string $numMobile): self
    {
        $this->numMobile = $numMobile;

        return $this;
    }

    public function getNumTelecopie(): ?string
    {
        return $this->numTelecopie;
    }

    public function setNumTelecopie(?string $numTelecopie): self
    {
        $this->numTelecopie = $numTelecopie;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePost(): ?int
    {
        return $this->codePost;
    }

    public function setCodePost(int $codePost): self
    {
        $this->codePost = $codePost;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * @return Collection|Rechargement[]
     */
    public function getRechargements(): Collection
    {
        return $this->rechargements;
    }

    public function addSolde(Rechargement $rechargement): self
    {
        if (!$this->rechargements->contains($rechargement)) {
            $this->rechargements[] = $rechargement;
            $rechargement->setClient($this);
        }

        return $this;
    }

    public function removeSolde(Rechargement $rechargement): self
    {
        if ($this->rechargements->removeElement($rechargement)) {
            // set the owning side to null (unless already changed)
            if ($rechargement->getClient() === $this) {
                $rechargement->setClient(null);
            }
        }

        return $this;
    }
}
