<?php

namespace App\Controller\BackOffice;

use App\Repository\UserRepository;
use App\Repository\DepotRepository;
use App\Repository\SoldesRepository;
use App\Repository\DepenseRepository;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @Route("/tableau-de-bord", name="dashboard")
     */
    public function index(UserRepository $userRepository,
                          DepotRepository $depotRepository,
                          DepenseRepository $depenseRepository,
                          SoldesRepository $soldesRepository,
                          RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        {
            return $this->render('BackOffice/dashboard/index.html.twig', [
                'users' => $userRepository->findAll(),
                'soldes'=> $soldesRepository->findAll(),
                'depots'=> $depotRepository->findAll(),
                'depenses'=> $depenseRepository->findAll(),
                'rechargements'=>$rechargementRepository->findAll()
            ]);
        }
    }
}
