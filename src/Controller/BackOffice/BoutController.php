<?php

namespace App\Controller\BackOffice;

use App\Entity\Bout;
use App\Form\BoutType;
use App\Repository\BoutRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BoutController extends AbstractController
{
    /**
     * @Route("/boutiques", name="bout")
     */
    public function index(BoutRepository $boutRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('BackOffice/bout/index.html.twig', [
            'bouts' => $boutRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ajouter-nouveau-boutique", name="bout_nouv")
     */
    public function Boutique(Request $request,EntityManagerInterface $manager)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }


        $bout = new Bout();
        $form = $this->createForm(BoutType::class, $bout);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
            {
                $manager->persist($bout);
                $manager->flush();
                $this->addFlash(
                    'success',
                    "La<strong>boutique</strong> a été bien enregistrer"
                );
                return $this->redirectToRoute('bout');
            }

        return $this-> render('BackOffice/bout/bout.html.twig',[
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/modification-de-boutique", name="bout_edit")
     */
    public function BoutEdit(Request $request,EntityManagerInterface $manager, Bout $bout)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(BoutType::class, $bout);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
            {
                $manager->persist($bout);
                $manager->flush();
                $this->addFlash(
                    'success',
                    "La<strong>boutique</strong> a été bien modifié avec succès"
                );
                return $this->redirectToRoute('bout');
            }

        return $this-> render('BackOffice/bout/edit.html.twig',[
            'form'=> $form->createView()
        ]);
    }

       /**
     * @Route("/{id}/Supprimer-un-boutique", name="bout_suppr")
     */
    public function delete(EntityManagerInterface $manager,Bout $bout)
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        $manager->remove($bout);
        $manager->flush();

        return $this->redirectToRoute('bout');
    }
}
