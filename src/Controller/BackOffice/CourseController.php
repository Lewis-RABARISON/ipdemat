<?php

namespace App\Controller\BackOffice;

use App\Repository\CourseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CourseController extends AbstractController
{
    /**
     * @Route("/mouvement", name="mouvement")
     */
    public function index(CourseRepository $courseRepository): Response
    {
        return $this->render('BackOffice/mouvement/index.html.twig', [
            'agents' => $courseRepository->findAll(),
        ]);
    }
}
