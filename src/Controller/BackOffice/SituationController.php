<?php

namespace App\Controller\BackOffice;

use App\Repository\UserRepository;
use App\Repository\DepotRepository;
use App\Repository\DepenseRepository;
use App\Repository\RechargementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SituationController extends AbstractController
{

    /**
     * @Route("/situations-des-agent", name="situation")
     */
    public function index()
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('BackOffice/situation/index.html.twig', [
            'controller_name' => 'SituationController',
        ]);
    }
    /**
     * @Route("/montants-recus", name="mont_rec")
     */
    public function recu(UserRepository $userRepository,
                          RechargementRepository $rechargementRepository): Response
    {
        if (!$this->getUser())
        {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('BackOffice/situation/recu.html.twig', [
            'users' => $userRepository->findAll(),
            'rechargements'=>$rechargementRepository->findAll()
        ]);
    }
   
   
    /**
     * @Route("/montants-deposer", name="mont_depot")
     */
    public function depo(UserRepository $userRepository,
                         DepotRepository $depotRepository): Response
    {
        return $this->render('BackOffice/situation/depot.html.twig', [
            'users' => $userRepository->findAll(),
            'depots'=>$depotRepository->findAll()
        ]);
    }
    /**
     * @Route("/montants-depenser", name="mont_depense")
     */
    public function Depense(UserRepository $userRepository,
                         DepenseRepository $depenseRepository): Response
    {
        return $this->render('BackOffice/situation/depense.html.twig', [
            'users' => $userRepository->findAll(),
            'depenses'=>$depenseRepository->findAll()
        ]);
    }
}
