<?php

namespace App\Controller\FrontOffice;

use App\Repository\CourseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CourseController extends AbstractController
{
    /**
     * @Route("/course", name="course")
     */
    public function index(CourseRepository $courseRepository): Response
    {
        return $this->render('FrontOffice/course/index.html.twig', [
            'courses' => $courseRepository->findAll(),
        ]);
    }
}
