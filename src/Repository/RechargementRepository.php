<?php

namespace App\Repository;

use App\Entity\Rechargement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rechargement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rechargement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rechargement[]    findAll()
 * @method Rechargement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RechargementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rechargement::class);
    }

    // /**
    //  * @return Rechargement[] Returns an array of Rechargement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rechargement
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
